<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return 'ini index Category';
    }

    public function create()
    {
        return view('category.create');
        // resources / views / category / create.blade.php
    }

    public function store(Request $request)
    {
        return $request->all();
    }

    public function show(Category $category)
    {
        //
    }

    public function edit(Category $category)
    {
        //
    }

    public function update(Request $request, Category $category)
    {
        //
    }

    public function destroy(Category $category)
    {
        //
    }
}
